import React, { useState } from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
import LoginPage from "./component/LoginPage";
import ItemPage from "./component/ItemPage";
import "./App.scss";

function App() {
  const [isLogin, changeLogin] = useState(false);
  const [user, setUser] = useState(undefined);
  return (
    <div className="App">
      <HashRouter>
        <Switch>
          <Route
            exact
            path="/"
            render={() => (
              <LoginPage
                isLogin={isLogin}
                changeLogin={changeLogin}
                setUser={setUser}
              />
            )}
          />
          <Route
            exact
            path="/items"
            render={() => (
              <ItemPage
                isLogin={isLogin}
                changeLogin={changeLogin}
                user={user}
                setUser={setUser}
              />
            )}
          />
        </Switch>
      </HashRouter>
    </div>
  );
}

export default App;
