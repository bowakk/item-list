import React, { useState } from "react";
import { Modal, Input } from "antd";

export default function ItemModal(props) {
  const [isEditMode, switchEditMode] = useState(props.modalType === "edit");
  function handleCancel() {
    let updateData = {};
    let input = document.getElementsByName("modal-input");
    [...input].forEach((item) => {
      updateData[item.id] = item.value;
    });
    if (!isEditMode) {
      props.updateItem(updateData);
    }
    props.close();
  }
  function editItem() {
    switchEditMode(true);
  }
  function saveItem() {
    switchEditMode(false);
  }
  return (
    <div>
      <Modal
        id="item-modal"
        title={isEditMode ? "Edit Item" : "View Item"}
        visible={props.visible}
        maskClosable={false}
        onOk={isEditMode ? saveItem : editItem}
        onCancel={handleCancel}
        okText={isEditMode ? "確定儲存" : "編輯修改"}
        cancelText="返回列表"
      >
        {Object.keys(props.item).map((key) => (
          <div key={`modal-${key}`}>
            <p className="modal-item-title">{key.toUpperCase()}</p>
            <Input
              type={key === "price" ? "number" : "text"}
              id={key}
              className="modal-item-input"
              name="modal-input"
              placeholder={key}
              defaultValue={props.item[key]}
              disabled={!isEditMode}
            />
          </div>
        ))}
      </Modal>
    </div>
  );
}
