import React, { useState } from "react";
import { Input, Icon } from "antd";
import { searchIdCheck } from "../../component/tools/validator";

export default function SearchBar(props) {
  const [error, setError] = useState("");
  function search() {
    let isSearchIdError = false;
    let searchId = document.getElementById("search-id").value.toLowerCase();
    let searchName = document.getElementById("search-name").value.toLowerCase();
    if (searchId !== "") {
      isSearchIdError = searchIdCheck(searchId);
      if (!isSearchIdError) {
        setError("");
        props.search(searchId, searchName);
      } else {
        setError("商品編號錯誤");
      }
    } else if (searchName !== "") {
      setError("");
      props.search(searchId, searchName);
    } else {
      setError("請輸入搜尋資料");
    }
  }
  return (
    <div className="search-bar">
      <span> 商品編號</span>
      <Input
        id="search-id"
        className="input-search"
        allowClear
        defaultValue={props.defaultValue}
        placeholder={props.placeholder}
        prefix={<Icon type="search" style={{ color: "rgba(0,0,0,.25)" }} />}
      />
      <span> 商品名稱</span>
      <Input
        id="search-name"
        className="input-search"
        allowClear
        defaultValue={props.defaultValue}
        placeholder={props.placeholder}
        prefix={<Icon type="search" style={{ color: "rgba(0,0,0,.25)" }} />}
      />
      <div className="search-icon">
        <i className="fas fa-search" onClick={search}></i>
      </div>
      <p className="error-msg">{error}</p>
    </div>
  );
}
