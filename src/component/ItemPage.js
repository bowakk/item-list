import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import { DATA } from "../const/data";
import SearchBar from "./Item/SearchBar";
import { Button, Table } from "antd";

import ItemModal from "./Item/ItemModal";

export default function ItemPage(props) {
  const [data, setData] = useState(DATA.data !== undefined && DATA.data);
  const [filterData, setFilterData] = useState(data !== undefined && data);
  const [editingIdx, setEditingIdx] = useState(undefined);
  const [showModal, switchModal] = useState(false);
  const [modalType, setModal] = useState(undefined);
  const [sortedInfo, setSortedInfo] = useState(undefined);
  const HEADER = [
    {
      title: "品牌",
      dataIndex: "brand",
      key: "brand",
    },
    {
      title: "名稱",
      dataIndex: "name",
      key: "name",
      sorter: (a, b) => a.name.localeCompare(b.name),
      sortOrder:
        sortedInfo !== undefined &&
        sortedInfo.columnKey === "name" &&
        sortedInfo.order,
      ellipsis: true,
    },
    {
      title: "編號",
      dataIndex: "id",
      key: "id",
      sorter: (a, b) => a.name.length - b.name.length,
      sortOrder:
        sortedInfo !== undefined &&
        sortedInfo.columnKey === "id" &&
        sortedInfo.order,
      ellipsis: true,
    },
    {
      title: "價錢",
      dataIndex: "price",
      key: "price",
      sorter: (a, b) => a.name.length - b.name.length,
      sortOrder:
        sortedInfo !== undefined &&
        sortedInfo.columnKey === "price" &&
        sortedInfo.order,
      ellipsis: true,
    },
    {
      title: "顏色",
      dataIndex: "color",
      key: "color",
    },
    {
      title: "尺寸",
      dataIndex: "size",
      key: "size",
    },
    {
      title: "動作",
      dataIndex: "action",
      key: "action",
      render: (value, row, index) => (
        <div>
          <i
            className="fa fa-eye icon"
            style={{ marginRight: "20px" }}
            onClick={() => openModal("view", index)}
          ></i>
          <i
            className="fas fa-edit icon"
            onClick={() => openModal("edit", index)}
          ></i>
        </div>
      ),
    },
  ];
  function logout() {
    props.setUser(undefined);
    props.changeLogin(false);
  }
  function handleChange(pagination, filters, sorter) {
    console.log("Various parameters", pagination, filters, sorter);
    setSortedInfo(sorter);
  }
  function openModal(type, idx) {
    setEditingIdx(idx);
    setModal(type);
    switchModal(true);
  }
  function closeModal() {
    setEditingIdx(undefined);
    setModal(undefined);
    switchModal(false);
  }
  function updateItem(item) {
    let newData = JSON.parse(JSON.stringify(data));
    newData[editingIdx] = item;
    setData(newData);
    setFilterData(newData);
  }
  function search(searchId = "", searchName = "") {
    let filterResult = [];
    if (searchId !== "" && searchName !== "") {
      filterResult = data.filter((item) => {
        return (
          item.id.toLowerCase().includes(searchId) &&
          item.name.toLowerCase().includes(searchName)
        );
      });
    } else if (searchId !== "") {
      filterResult = data.filter((item) => {
        return item.id.toLowerCase().includes(searchId);
      });
    } else if (searchName !== "") {
      filterResult = data.filter((item) => {
        return item.name.toLowerCase().includes(searchName);
      });
    }
    setFilterData(filterResult);
  }
  return props.isLogin ? (
    <div className="item-content">
      <div className="user-bar">
        <span>{`Welcome, ${props.user}`}</span>
        <Button type="primary" onClick={logout}>
          登出
        </Button>
      </div>
      <SearchBar placeholder="搜尋" search={search} />
      <span>共{data.length}筆資料</span>
      <Table
        id="item-table"
        rowKey={(record, index) => index}
        columns={HEADER}
        dataSource={filterData}
        pagination={{
          defaultPageSize: 20,
          pageSizeOptions: ["20", "40", "100"],
          showSizeChanger: true,
          position: "top",
        }}
        onChange={handleChange}
      />
      {showModal && (
        <ItemModal
          visible={showModal}
          modalType={modalType}
          item={data[editingIdx]}
          close={closeModal}
          updateItem={updateItem}
        />
      )}
    </div>
  ) : (
    <Redirect push to="/" />
  );
}
