import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import { Button, Input, Tooltip, Icon } from "antd";
import { accountCheck, passwordCheck } from "../component/tools/validator";

export default function LoginPage(props) {
  const [error, setError] = useState("");
  function login() {
    let account = document.getElementById("account").value;
    let password = document.getElementById("password").value;
    let isAccountError = accountCheck(account);
    let isPasswordError = passwordCheck(password);
    if (!isAccountError && !isPasswordError) {
      setError("");
      props.setUser(account);
      props.changeLogin(true);
    } else {
      if (isAccountError && isPasswordError) {
        setError("帳號和密碼錯誤");
      } else if (!isAccountError) {
        setError("帳號錯誤");
      } else {
        setError("密碼錯誤");
      }
    }
  }
  return !props.isLogin ? (
    <div className="login-content">
      <p className="login-title">會員登入</p>
      <Input
        id="account"
        className="login-input"
        placeholder="使用者帳號-3~20碼"
        prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
        suffix={
          <Tooltip title="帳號首字母需英文, 密碼需英數混合">
            <Icon type="info-circle" style={{ color: "rgba(0,0,0,.45)" }} />
          </Tooltip>
        }
      />
      <Input.Password
        id="password"
        className="login-input"
        placeholder="密碼-6~20碼，英文字母需區分大小寫"
      />
      <p className="error-msg">{error}</p>
      <Button type="primary" onClick={login}>
        登入
      </Button>
    </div>
  ) : (
    <Redirect push to="/items" />
  );
}
