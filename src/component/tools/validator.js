export function accountCheck(input) {
  let regex = /^[a-z||A-Z]{1}[a-z||A-Z||0-9]{4,}$/;
  let result = regex.test(input);
  if (!result) {
    return true;
  }
  return false;
}

export function passwordCheck(input) {
  let regex = /^(?=.*[a-z||A-Z])(?=.*[0-9])[a-z||A-Z||0-9]{6,20}$/;
  let result = regex.test(input);
  if (!result) {
    return true;
  }
  return false;
}

export function searchIdCheck(input) {
  let regex = /^(?=.*[a-z||A-Z])(?=.*[0-9])[a-z||A-Z||0-9]{2,}$/;
  let result = regex.test(input);
  if (!result) {
    return true;
  }
  return false;
}
